package com.sensicardiac.sensixvi.Controller;


import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.github.lzyzsd.circleprogress.DonutProgress;

import com.sensicardiac.sensixvi.PagerAdapter.MyPagerAdapter;
import com.sensicardiac.sensixvi.Playback.PlaybackListener;
import com.sensicardiac.sensixvi.Playback.PlaybackThread;
import com.sensicardiac.sensixvi.R;
import com.sensicardiac.sensixvi.SQL_LITE.DB_Handler;
import com.sensicardiac.sensixvi.SQL_LITE.PATIENT;
import com.sensicardiac.waveform.WaveformView;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;


import static com.sensicardiac.sensixvi.Recording_Management.utils.File_Util.Del_Directory;
import static com.sensicardiac.sensixvi.Stream.Stream.findAudioParams;


public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private InputIntentReceiver myReceiver;
    public static boolean IsPlugged = false;

    public static Context getmContext() {
        return mContext;
    }

    public static void setmContext(Context mContext) {
        MainActivity.mContext = mContext;
    }

    public static Context mContext;

    public static ImageView hr_falsher;

    public static TextView hr_text;

    public static DonutProgress donutProgress;
    public static WaveFormView1_1 wavform;
    public static WaveformView mPlaybackView;
    public static PlaybackThread mPlaybackThread;

    public static ImageButton play_btn;
    public static ImageButton pause_button;
    public static Spinner speed_spinner;
    public static ImageButton reset_btn;
    public static ImageButton session_btn;
    public static ImageButton history_btn;
    public static ImageButton settings_btn;
    public static FragmentManager fragmentManager;
    public static Fragment history;
    public static Fragment settings;
    public static Runnable graph_mTimer;
    public static Handler graph_mHandler;
    public static int graph_timer = 0;
    public static int val =0;
    public static Thread run_updates;
    public static boolean FILE_SAVED = false;
    public static int HR_FLASHER_VAL = 0;
    public static boolean NEW_FILE = true;
    public static int PATIENT_SELECTED = 0;
    public static boolean DB_SHOW = false;
 //   public static SpinnerAdapter speed_spinner_adapter;
    public static int speed = 1;

    public static ViewPager mvPager;
    public static MyPagerAdapter myPagerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Pager
        mvPager = (ViewPager)findViewById(R.id.vpPager);
        myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        mvPager.setAdapter(myPagerAdapter);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.HEADSET_PLUG");
        myReceiver = new InputIntentReceiver();
        registerReceiver( myReceiver, intentFilter);
        setmContext(this.getApplicationContext());
        com.sensicardiac.sensixvi.Stream.Stream.Initialize_Recorder();
      com.sensicardiac.sensixvi.DSP.DSP_Variables.startDSP();
        int test=findAudioParams();
        com.sensicardiac.sensixvi.DSP.DSP_Variables.downsample=new com.sensicardiac.sensixvi.DSP.uni_downsampler(2,0.05);
        Del_Directory();
        listeners();
    }

    public static TextView db_name;
    public static TextView db_age;
    public static TextView db_pos;
    public static TextView db_apex;
    public static String _p_name;
    public static String _p_age;
    public static String _p_pos;
    public static String _p_apex;


    public  void listeners(){

    }

    public static void set_DB_ITEM(){
        DB_Handler db = new DB_Handler(MainActivity.getmContext());
        PATIENT p = db.getPatient(PATIENT_SELECTED);
       _p_name = p.get_Name();
       _p_age = String.valueOf(p.get_Age());
       _p_pos = p.get_P_Position();
       _p_apex = p.get_P_Apex_Disp();


        db.close();
    }

    public void Message(String message){
        Toast.makeText(this.getApplicationContext(),message,Toast.LENGTH_LONG).show();

    }
    private class InputIntentReceiver extends BroadcastReceiver {
        @Override public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
                int state = intent.getIntExtra("state", -1);
                switch (state) {
                    case 0:
                        Message("Headset or Device unplugged");
                        IsPlugged = false;
                        break;
                    case 1:
                        Message("Headset or Device is plugged");
                        IsPlugged = true;
                        break;
                    default:
                        Message("I have no idea what the state is");
                        IsPlugged = false;
                }
            }
        }
    }

    @Override public void onPause() {
        try{
        unregisterReceiver(myReceiver);}catch (Exception e){}
        super.onPause();
    }

    @Override public void onDestroy(){
        unregisterReceiver(myReceiver);
        super.onDestroy();
    }

    @Override public void onResume(){
        myReceiver = new InputIntentReceiver();
        super.onResume();
    }

    public static void Value_Updater(){
        graph_timer = 0;
        val =0;
        graph_mTimer = new Runnable() {
            @Override
            public void run() {
                if (com.sensicardiac.sensixvi.Stream.Stream.isRecording()) {
                    com.sensicardiac.sensixvi.Stream.Stream.setGraph_data_ready(false);

                    MainActivity.wavform.updateAudioData(com.sensicardiac.sensixvi.DSP.DSP_Variables.downsample.getGraph());
                } graph_mHandler.postDelayed(this, 50);
            }

        };
        graph_mHandler.postDelayed(graph_mTimer, 50);
    }

    public static void Playback_Audio_File_Graph(){
        short[] samples = null;
        mPlaybackView.setChannels(1);
        mPlaybackView.setSampleRate(4000);
        try {
            samples = getAudioSample();
            mPlaybackView.setSamples(samples);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mPlaybackThread = new PlaybackThread(samples, new PlaybackListener() {
            @Override
            public void onProgress(int progress) {
                mPlaybackView.setMarkerPosition(progress);
            }

            @Override
            public void onCompletion() {
                mPlaybackView.setMarkerPosition(mPlaybackView.getAudioLength());
                mPlaybackThread.donePlayback();
            }
        });

    }
    private static short[] getAudioSample() throws IOException {

        File file = new File("/sdcard/sensi_x/tempfile.wav");
        FileInputStream fileInputStream = new FileInputStream(file);
        InputStream is = fileInputStream;
        byte[] data;
        try {
            data = org.apache.commons.io.IOUtils.toByteArray(is);
            for (int x = 0;x < 44;x++){
                data[x] = 0;
            }
        } finally {
            if (is != null) {
                is.close();
            }
        }

        ShortBuffer sb = ByteBuffer.wrap(data).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer();
        short[] samples = new short[sb.limit()];
        sb.get(samples);
        return samples;
    }
    public static void Setpage(int num){
        mvPager.setCurrentItem(num);
    }
    }




