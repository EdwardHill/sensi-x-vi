package com.sensicardiac.sensixvi.Controller;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.SurfaceView;


import com.sensicardiac.sensixvi.R;

import java.util.LinkedList;

/**
 * Created by Edward on 16/08/26.
 */
public class WaveFormView1_1 extends SurfaceView {

    // The number of buffer frames to keep around (for a nice fade-out visualization).
    private static final int HISTORY_SIZE = 1;

    // To make quieter sounds still show up well on the display, we use +/- 8192 as the amplitude
    // that reaches the top/bottom of the view instead of +/- 32767. Any samples that have
    // magnitude higher than this limit will simply be clipped during drawing.
    private static final float MAX_AMPLITUDE_TO_DRAW = 16000f;

    // The queue that will hold historical audio data.
    private final LinkedList<int[]> mAudioData;

    private final Paint mPaint;
    private final Paint mPaint1;

    public WaveFormView1_1(Context context) {
        this(context, null, 0);
    }

    public WaveFormView1_1(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public WaveFormView1_1(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        mAudioData = new LinkedList<int[]>();

        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(Color.WHITE);
        mPaint.setStrokeWidth(0);
        mPaint.setAntiAlias(true);
        mPaint1 = new Paint();
        mPaint1.setStyle(Paint.Style.STROKE);
        mPaint1.setColor(Color.WHITE);
        mPaint1.setStrokeWidth(0);
        mPaint1.setAntiAlias(true);

    }

    /**
     * Updates the waveform view with a new "frame" of samples and renders it. The new frame gets
     * added to the front of the rendering queue, pushing the previous frames back, causing them to
     * be faded out visually.
     *
     * @param buffer the most recent buffer of audio samples
     */
    public synchronized void updateAudioData(int[] buffer) {
        int[] newBuffer;

        // We want to keep a small amount of history in the view to provide a nice fading effect.
        // We use a linked list that we treat as a queue for this.


        if (mAudioData.size() == HISTORY_SIZE) {
            newBuffer = mAudioData.removeFirst();
            System.arraycopy(buffer, 0, newBuffer, 0, buffer.length);
        } else
        {
            newBuffer = buffer.clone();
        }

        mAudioData.addLast(newBuffer);

        // Update the display.
        Canvas canvas = getHolder().lockCanvas();
        if (canvas != null) {
            drawWaveform(canvas);
            getHolder().unlockCanvasAndPost(canvas);
        }
    }

    /**
     * Repaints the view's surface.
     *
     * @param canvas the {@link Canvas} object on which to draw
     */
    private void drawWaveform(Canvas canvas) {
        // Clear the screen each time because SurfaceView won't do this for us.
        canvas.drawColor(Color.WHITE);

        float width = getWidth();
        float height = getHeight();
        float centerY = height / 2;

        // We draw the history from oldest to newest so that the older audio data is further back
        // and darker than the most recent data.
        int colorDelta = 255 / (HISTORY_SIZE + 1);
        int brightness = colorDelta;

        for (int[] buffer : mAudioData) {
            mPaint.setColor(getResources().getColor(R.color.colorPrimary));
            mPaint1.setColor(getResources().getColor(R.color.colorPrimary));
            mPaint1.setStrokeWidth(0.5f);
            mPaint.setStrokeWidth(0.5f);
            float lastX = -1;
            float lastY = -1;

            // For efficiency, we don't draw all of the samples in the buffer, but only the ones
            // that align with pixel boundaries.
            for (int x = 0; x < width; x++) {
                int index = (int) ((x / width) * buffer.length);
                int sample = buffer[index];
                float y = (sample / MAX_AMPLITUDE_TO_DRAW) * centerY + centerY;
                float y1 = (-1 * (sample / MAX_AMPLITUDE_TO_DRAW)) * centerY + centerY;
                if (lastX != -1) {
                    canvas.drawLine(lastX, lastY, x, y, mPaint);
                    canvas.drawLine(lastX, lastY, x, y1, mPaint1);
                }

                lastX = x;
                lastY = y;
            }

            brightness += colorDelta;
        }
    }
}
