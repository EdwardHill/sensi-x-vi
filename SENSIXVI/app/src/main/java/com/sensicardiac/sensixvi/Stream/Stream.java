package com.sensicardiac.sensixvi.Stream;


import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.view.View;

import java.lang.reflect.Method;
import java.nio.Buffer;
import java.nio.ShortBuffer;
import java.util.Calendar;
import java.util.Date;


import com.sensicardiac.sensixvi.Controller.MainActivity;
import com.sensicardiac.sensixvi.DSP.DSP_Variables;
import com.sensicardiac.sensixvi.R;


import static com.sensicardiac.sensixvi.Controller.MainActivity.HR_FLASHER_VAL;
import static com.sensicardiac.sensixvi.DSP.DSP_Variables.downsample;
import static com.sensicardiac.sensixvi.DSP.DSP_Variables.hrCalc;
import static com.sensicardiac.sensixvi.Stream.Audio_File_Saver.Save_Audio;

/**
 * Created by Edward on 2017/02/13.
 */

public class Stream {

    private static byte[] audio_packet;
    private static int bufferSize;
    private static int number_of_packets_to_receive;
    private static byte[] final_audio_packets;
    private static boolean recording_busy;
    private static String finished;
    private static double graph_Value;
    private static boolean recording_done = false;
    private static byte upper_byte = 0;
    private static byte lower_byte = 0;
    private static double g_val_1;
    private static double g_val_2;
    private static double g_val_3;
    private static double g_val_4;
    private static double g_val_5;
    public static byte[] test_graph = new byte[16384];
    private static boolean graph_data_ready;
    private static boolean recording;
    private static boolean stream_running;

    public static int getSampleRate() {
        return sampleRate;
    }

    public static void setSampleRate(int sampleRate) {
        Stream.sampleRate = sampleRate;
    }

    private static int sampleRate;

    public static boolean isStream_running() {
        return stream_running;
    }

    public static void setStream_running(boolean stream_running) {
        Stream.stream_running = stream_running;
    }

    public static boolean isRecording() {
        return recording;
    }

    public static void setRecording(boolean recording) {
        Stream.recording = recording;
    }

    public static byte[] getFinal_audio_packets() {
        return final_audio_packets;
    }

    public static void setFinal_audio_packets(byte[] final_audio_packets) {
        Stream.final_audio_packets = final_audio_packets;
    }


    public static byte[] getAudio_packet() {
        return audio_packet;
    }

    public static void setAudio_packet(byte[] audio_packet) {
        Stream.audio_packet = audio_packet;
    }

    public static int getBufferSize() {
        return bufferSize;
    }

    public static void setBufferSize(int bufferSize) {
        Stream.bufferSize = bufferSize;
    }

    public static int getNumber_of_packets_to_receive() {
        return number_of_packets_to_receive;
    }

    public static void setNumber_of_packets_to_receive(int number_of_packets_to_receive) {
        Stream.number_of_packets_to_receive = number_of_packets_to_receive;
    }

    public static boolean isRecording_busy() {
        return recording_busy;
    }

    public static void setRecording_busy(boolean recording_busy) {
        Stream.recording_busy = recording_busy;
    }

    public static String getFinished() {
        return finished;
    }

    public static void setFinished(String finished) {
        Stream.finished = finished;
    }

    public static double getGraph_Value() {
        return graph_Value;
    }

    public static void setGraph_Value(double graph_Value) {
        Stream.graph_Value = graph_Value;
    }

    public static boolean isRecording_done() {
        return recording_done;
    }

    public static void setRecording_done(boolean recording_done) {
        Stream.recording_done = recording_done;
    }

    public static byte getUpper_byte() {
        return upper_byte;
    }

    public static void setUpper_byte(byte upper_byte) {
        Stream.upper_byte = upper_byte;
    }

    public static byte getLower_byte() {
        return lower_byte;
    }

    public static void setLower_byte(byte lower_byte) {
        Stream.lower_byte = lower_byte;
    }

    public static double getG_val_1() {
        return g_val_1;
    }

    public static void setG_val_1(double g_val_1) {
        Stream.g_val_1 = g_val_1;
    }

    public static double getG_val_2() {
        return g_val_2;
    }

    public static void setG_val_2(double g_val_2) {
        Stream.g_val_2 = g_val_2;
    }

    public static double getG_val_3() {
        return g_val_3;
    }

    public static void setG_val_3(double g_val_3) {
        Stream.g_val_3 = g_val_3;
    }

    public static double getG_val_4() {
        return g_val_4;
    }

    public static void setG_val_4(double g_val_4) {
        Stream.g_val_4 = g_val_4;
    }

    public static double getG_val_5() {
        return g_val_5;
    }

    public static void setG_val_5(double g_val_5) {
        Stream.g_val_5 = g_val_5;
    }

    public static byte[] getTest_graph() {
        return test_graph;
    }

    public static void setTest_graph(byte[] test_graph) {
        Stream.test_graph = test_graph;
    }

    public static boolean isGraph_data_ready() {
        return graph_data_ready;
    }

    public static void setGraph_data_ready(boolean graph_data_ready) {
        Stream.graph_data_ready = graph_data_ready;
    }

    private static Async_Analog_Record Async_Analog;

    public static void Start_Analog_Recording() {
        setRecording_done(false);
        MainActivity.donutProgress.setProgress(0);
        setAudio_packet(new byte[getBufferSize()]);
        setNumber_of_packets_to_receive(500);
        setFinal_audio_packets(new byte[getBufferSize() * getNumber_of_packets_to_receive()]);
        setRecording_busy(true);
        //  MainDisplay.Record_Stop.setEnabled(false);
        try {
            recorder = null;
            recorder = findAudioRecord();
            //       MainDisplay.Record_Stop.setEnabled(true);
            Async_Analog = null;
            Async_Analog = new Async_Analog_Record();
            Async_Analog.execute("Start");
//            String test =  Async_Analog.execute("Start");
//            HockeyLog.info("Setup", "Buffer size: " + getBufferSize());
//            HockeyLog.info("Setup", "Number of packets: " + getNumber_of_packets_to_receive());


        } catch (Exception e) {
            //        HockeyLog.error("Recorder",e);
        }

    }

    public static void Stop_Analog_Recording() {
        setRecording_busy(false);
        if (Async_Analog.getStatus() == AsyncTask.Status.RUNNING) {
            Log.d("Async : ", "Running");

        }
        if (Async_Analog.getStatus() == AsyncTask.Status.PENDING) {

            Log.d("Async : ", "Pending");
        }
        if (Async_Analog.getStatus() == AsyncTask.Status.FINISHED) {
            Log.d("Async : ", "Finished");
            Async_Analog.cancel(true);
            Async_Analog = null;
        }
        if (String.valueOf(AsyncTask.Status.FINISHED) == getFinished()) {
            //       MainDisplay.progress_load.setVisibility(View.VISIBLE);
            Log.d("Async : ", "Finished");
            Async_Analog.cancel(true);
            Async_Analog = null;
            setRecording(false);
            setStream_running(false);
            //   MainDisplay.Record_Stop.setImageResource(R.drawable.ic_record);
            //  MainDisplay.Record_Stop_Text.setText("REC");
            //  MainDisplay.final_graph();
            setRecording_done(true);
            // MainDisplay.hr_flasher.setVisibility(View.VISIBLE);
        }
    }

    private static AudioRecord recorder;
    private static AudioManager audioManager;
    private static int counter;
    private static int byte_counter;
    public static byte[] total_packets;
    private static boolean audio_recorder_selected = false;
    public static int offSet;
    private static boolean flash = false;
    private static Calendar c;
    private static int Start_seconds;
    private static int next_second;
    private static float val = 0;
    private static int prog_int = 0;
    public static void Initialize_Recorder(){
        recorder = findAudioRecord();

    }

    public static class Async_Analog_Record extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {

            setFinished("RUNNING");
            val = 0;
            prog_int = 0;
            setRecording(true);
            recorder = findAudioRecord();
            recorder.startRecording();
            byte[] buffer = new byte[getBufferSize()];
            total_packets = getFinal_audio_packets();

           DSP_Variables.reset();
            int read = 0;
            offSet = 0;
            counter = 0;
            byte_counter = 0;
            int graph_count = 0;
            int time_count = 0;
            byte[] graph_byte = new byte[4 * getSampleRate()];
            c = Calendar.getInstance();
            Start_seconds = c.get(Calendar.SECOND);

            while (isRecording_busy()) {
                read = recorder.read(buffer, 0, buffer.length);
                double temp = 0;


                for (int i = 0; i < buffer.length; i++) {

                    total_packets[i + offSet] = buffer[i];
                    if (i % 2 == 0) {
                        try{
                        downsample.add_val(buffer[i], buffer[i + 1]);}catch (Exception e){
                            Log.e ("EXCEPTION ASYNC","e");
                        }
                    }
//                        if(i+graph_count<graph_byte.length) {
//                            graph_byte[i + graph_count] = buffer[i];
//                        }
                    byte_counter++;


                }
//                publishProgress("Graph");

                if (time_count == counter){
                    time_count += 10;
//                    prog_int = 1;
                    publishProgress("Ready");
                }
                counter++;
//                time_count+=getBufferSize();
//                graph_count +=getBufferSize();
//                if (time_count >=graph_byte.length ){
//                    setGraph_data_ready(true);
//                    test_graph = graph_byte;
//                    time_count =0;
//                    graph_count = 0;}


                offSet += getBufferSize();

//                setG_val_1(Math.abs(0)) ;
//                        + buffer[1] << 8);

//                for (int k = 0; k < 128;k+=2){
//                    temp = 0;
//                    temp += (buffer[k+1]<<8)|buffer[k];
//
//                    setLower_byte(buffer[k]);
//                    setUpper_byte(buffer[k + 1]);
//                    add_to_graph();
//                }
//                temp = temp /64;


//                Log.d("Counter : ", String.valueOf(counter));
//                Log.d("ByteCounter : ", String.valueOf(byte_counter));
                if (counter == getNumber_of_packets_to_receive()) {
                    setRecording_busy(false);
                }
            }
            recorder.stop();
            recorder.release();
            recorder = null;
            setRecording(false);
            Log.d("Done","Done");
//            boolean testOdd = isOdd(byte_counter);
//            int final_count = 0;
//            if (testOdd){
//                final_count = byte_counter - 1;
//            } else if (!testOdd){
//                final_count = byte_counter;
//            }
//
//            total_bytes = new byte[final_count];
//            for (int i =0;i<final_count;i++){
//                total_bytes[i] = total_packets[i];
//
//            }
//            Log.i("Record","Signal length: "+total_bytes.length);
            setFinal_audio_packets(downsample.getByteArray());
            setFinished("FINISHED");
            return getFinished();
        }

        @Override
        protected void onProgressUpdate(String... progress) {
//            if (prog_int == 1){
            if (flash){
            MainActivity.hr_falsher.setImageResource(R.drawable.ic_heart_empty_small);
            flash = false;}
            else if(!flash){
                MainActivity.hr_falsher.setImageResource(R.drawable.ic_heart);
                flash = true;
            }
            int inst = (int) hrCalc.stats.getMovingAverage();
            MainActivity.hr_text.setText(String.valueOf(inst));
            HR_FLASHER_VAL = inst;
            c = Calendar.getInstance();
            next_second = c.get(Calendar.SECOND);
            if (next_second == 0){
                Start_seconds = -1;
            }
            Log.d("Seconds","Start :" + Start_seconds + "<<<<>>>>> Now : " + next_second);
            if(next_second > Start_seconds){
                 val += 5;
                Start_seconds = next_second;
                MainActivity.donutProgress.setProgress(val);
            }
//            prog_int = 0;
//        }

        }

        @Override
        protected void onPostExecute(String result) {

            com.sensicardiac.sensixvi.Controller.MainActivity.wavform.setVisibility(View.GONE);
            MainActivity.hr_falsher.setImageResource(R.drawable.ic_heart);
            Save_Audio(getFinal_audio_packets());
            MainActivity.donutProgress.setProgress(100);
            Stop_Analog_Recording();
        }


    }

    private static byte[] total_bytes;

    private static boolean isOdd(int count) {
        return (count & 0x01) != 0;
    }

    private static int[] mSampleRates = new int[]{4000, 8000, 11025, 22050, 16000, 32000, 44000, 44100, 96000};

    //    private static int[] mSampleRates = new int[] {32000};
    public static AudioRecord findAudioRecord() {
        AudioManager manager = (AudioManager) com.sensicardiac.sensixvi.Controller.MainActivity.getmContext().getSystemService(Context.AUDIO_SERVICE);
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.DONUT) {
                    /* see AudioService.setRouting
		    		* Use MODE_INVALID to force headset routing change */
            manager.setRouting(AudioManager.MODE_INVALID, AudioManager.ROUTE_HEADSET, AudioManager.ROUTE_HEADSET);
        } else {
            setDeviceConnectionState(DEVICE_IN_WIRED_HEADSET, DEVICE_STATE_AVAILABLE, "");
            setDeviceConnectionState(DEVICE_OUT_WIRED_HEADSET, DEVICE_STATE_AVAILABLE, "");
        }
        for (int rate : mSampleRates) {
            for (short audioFormat : new short[]{AudioFormat.ENCODING_PCM_16BIT}) {
                for (short channelConfig : new short[]{AudioFormat.CHANNEL_IN_MONO}) {
                    try {
                        Log.d("C.TAg", "Attempting rate " + rate + "Hz, bits: " + audioFormat + ", channel: "
                                + channelConfig);
                        int bufferSize = AudioRecord.getMinBufferSize(rate, channelConfig, audioFormat);

                        if (bufferSize != AudioRecord.ERROR_BAD_VALUE) {
                            // check if we can instantiate and have a success
                            setBufferSize(bufferSize);
                            AudioRecord recorder = new AudioRecord(MediaRecorder.AudioSource.DEFAULT, rate, channelConfig, audioFormat, bufferSize);
                            Log.e("SAMPLERATE", "SAMPLE RATE : " + rate + " Channel : " + channelConfig + " FOrmat : " + audioFormat + " buff_size " + bufferSize);
                            if (recorder.getState() == AudioRecord.STATE_INITIALIZED)
//                                setDeviceConnectionState(DEVICE_IN_WIRED_HEADSET, DEVICE_STATE_UNAVAILABLE, "");

                                return recorder;
                        }
                    } catch (Exception e) {
                        Log.e("C.TAg", rate + "Exception, keep trying.", e);
                    }
                }
            }
        }
        return recorder;
    }


    public static int findAudioParams() {
        AudioManager manager = (AudioManager) com.sensicardiac.sensixvi.Controller.MainActivity.getmContext().getSystemService(Context.AUDIO_SERVICE);
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.DONUT) {
		    		/* see AudioService.setRouting
		    		* Use MODE_INVALID to force headset routing change */
            manager.setRouting(AudioManager.MODE_INVALID, AudioManager.ROUTE_HEADSET, AudioManager.ROUTE_HEADSET);
        } else {
            setDeviceConnectionState(DEVICE_IN_WIRED_HEADSET, DEVICE_STATE_AVAILABLE, "");
            setDeviceConnectionState(DEVICE_OUT_WIRED_HEADSET, DEVICE_STATE_AVAILABLE, "");
        }
        for (int rate : mSampleRates) {
            for (short audioFormat : new short[]{AudioFormat.ENCODING_PCM_16BIT}) {
                for (short channelConfig : new short[]{AudioFormat.CHANNEL_IN_MONO}) {
                    try {
                        Log.d("C.TAg", "Attempting rate " + rate + "Hz, bits: " + audioFormat + ", channel: "
                                + channelConfig);
                        int bufferSize = AudioRecord.getMinBufferSize(rate, channelConfig, audioFormat);

                        if (bufferSize != AudioRecord.ERROR_BAD_VALUE) {
                            // check if we can instantiate and have a success
                            AudioRecord recorder = new AudioRecord(MediaRecorder.AudioSource.DEFAULT, rate, channelConfig, audioFormat, bufferSize);
                            Log.e("SAMPLERATE", "SAMPLE RATE : " + rate + " Channel : " + channelConfig + " FOrmat : " + audioFormat + " buff_size " + bufferSize);
                            if (recorder.getState() == AudioRecord.STATE_INITIALIZED)
//                                setDeviceConnectionState(DEVICE_IN_WIRED_HEADSET, DEVICE_STATE_UNAVAILABLE, "");
                                setSampleRate(rate);
                            setBufferSize(bufferSize);
                            return 1;
                        }
                    } catch (Exception e) {
                        Log.e("C.TAg", rate + "Exception, keep trying.", e);
                    }
                }
            }
        }
        return 0;
    }
//    public static void add_to_graph() {
//        try{
//            double in = getByteCombination();
////        value = envelope.add_val(in);
//            hrCalc.add_val(in);
//        } catch (Exception e){
//            Log.d("hrcalc ",String.valueOf(e) );
//        }
//    }

    private static void setDeviceConnectionState(final int device, final int state, final String address) {
        try {
            Class<?> audioSystem = Class.forName("android.media.AudioSystem");
            Method setDeviceConnectionState = audioSystem.getMethod(
                    "setDeviceConnectionState", int.class, int.class, String.class);

            setDeviceConnectionState.invoke(audioSystem, device, state, address);
        } catch (Exception e) {
            Log.e("Error on Device", "setDeviceConnectionState failed: " + e);
        }


    }

    private static final int DEVICE_IN_WIRED_HEADSET = 0x400000;
    private static final int DEVICE_OUT_EARPIECE = 0x1;
    private static final int DEVICE_OUT_WIRED_HEADSET = 0x4;
    private static final int DEVICE_STATE_UNAVAILABLE = 0;
    private static final int DEVICE_STATE_AVAILABLE = 1;
}

