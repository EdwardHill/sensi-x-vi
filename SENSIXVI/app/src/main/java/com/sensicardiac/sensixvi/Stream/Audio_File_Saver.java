package com.sensicardiac.sensixvi.Stream;

import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import com.sensicardiac.sensixvi.Controller.MainActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static com.sensicardiac.sensixvi.Controller.MainActivity.Setpage;

/**
 * Created by Edward on 16/08/17.
 */
public class Audio_File_Saver {
    public static void Save_Audio(byte[] byte_packets){
        WaveHeader hdr;
//        if (getInput_method() == Input_Method.Bluetooth) {
            hdr = new WaveHeader(WaveHeader.FORMAT_PCM, (short) 1, 4000, (short) 16, byte_packets.length);
//        }
//        else if (getInput_method() == Input_Method.Analog){
//            WaveHeader hdr = new WaveHeader(WaveHeader.FORMAT_PCM, (short) 1, Global_Variables.getSampleRate(), (short) 16, byte_packets.length);
//        }
        File temp = new File(Environment.getExternalStorageDirectory() + "/sensi_x/tempfile.wav");
        File dir = new File(Environment.getExternalStorageDirectory() + "/sensi_x");
        if (!dir_exists(dir.getPath())){
            File directory = new File(String.valueOf(dir));
            directory.mkdirs();
        }
        if (!temp.exists()){
            try {
                temp.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (temp.exists()){
            temp.delete();
            try {
                temp.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            FileOutputStream fos = new FileOutputStream(temp);
            hdr.write(fos);
            fos.write(byte_packets);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                com.sensicardiac.sensixvi.Controller.MainActivity.Playback_Audio_File_Graph();
                com.sensicardiac.sensixvi.Controller.MainActivity.mPlaybackView.setVisibility(View.VISIBLE);
                com.sensicardiac.sensixvi.Controller.MainActivity.wavform.setVisibility(View.GONE);
            //    com.sensicardiac.sensixvi.Controller.MainActivity.sv.setSaving(true);
                MainActivity.FILE_SAVED = true;
                Log.d("FILE SAVED","<<<<<<<<<<<<<<<FILE SAVED>>>>>>>>>>>>>>"+MainActivity.FILE_SAVED);

                Setpage(2);
            }
        }, 500);


    }

    public static boolean dir_exists(String dir_path)
    {
        boolean ret = false;
        File dir = new File(dir_path);
        if(dir.exists() && dir.isDirectory())
            ret = true;
        return ret;
    }
}
