package com.sensicardiac.sensixvi.Controller;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.sensicardiac.sensixvi.R;
import com.sensicardiac.sensixvi.Recording_Management.Recoring_Add;
import com.sensicardiac.waveform.WaveformView;

import java.util.ArrayList;
import java.util.List;

import static com.sensicardiac.sensixvi.Controller.MainActivity.DB_SHOW;
import static com.sensicardiac.sensixvi.Controller.MainActivity.FILE_SAVED;
import static com.sensicardiac.sensixvi.Controller.MainActivity.HR_FLASHER_VAL;
import static com.sensicardiac.sensixvi.Controller.MainActivity.NEW_FILE;
import static com.sensicardiac.sensixvi.Controller.MainActivity.Setpage;
import static com.sensicardiac.sensixvi.Controller.MainActivity.Value_Updater;
import static com.sensicardiac.sensixvi.Controller.MainActivity.donutProgress;
import static com.sensicardiac.sensixvi.Controller.MainActivity.graph_mHandler;
import static com.sensicardiac.sensixvi.Controller.MainActivity.graph_timer;
import static com.sensicardiac.sensixvi.Controller.MainActivity.history_btn;
import static com.sensicardiac.sensixvi.Controller.MainActivity.hr_falsher;
import static com.sensicardiac.sensixvi.Controller.MainActivity.hr_text;
import static com.sensicardiac.sensixvi.Controller.MainActivity.mPlaybackThread;
import static com.sensicardiac.sensixvi.Controller.MainActivity.mPlaybackView;
import static com.sensicardiac.sensixvi.Controller.MainActivity.pause_button;
import static com.sensicardiac.sensixvi.Controller.MainActivity.play_btn;
import static com.sensicardiac.sensixvi.Controller.MainActivity.reset_btn;
import static com.sensicardiac.sensixvi.Controller.MainActivity.run_updates;
import static com.sensicardiac.sensixvi.Controller.MainActivity.session_btn;
import static com.sensicardiac.sensixvi.Controller.MainActivity.settings_btn;
import static com.sensicardiac.sensixvi.Controller.MainActivity.speed;
import static com.sensicardiac.sensixvi.Controller.MainActivity.speed_spinner;

import static com.sensicardiac.sensixvi.Controller.MainActivity.val;
import static com.sensicardiac.sensixvi.Controller.MainActivity.wavform;
import static com.sensicardiac.sensixvi.Recording_Management.Recoring_Add.populate_fields;
import static com.sensicardiac.sensixvi.Stream.Stream.isRecording;

/**
 * Created by Edward on 2017/02/22.
 */

public class Main_Fragment extends Fragment

    {

//        public static Main_Fragment newInstance(){
//            Main_Fragment f = new Main_Fragment();
//        return f;
//    }
public  View v;
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
            DB_SHOW = false;
            v = inflater.inflate(R.layout.main_act,container,false);
            hr_falsher = (ImageView)v.findViewById(R.id.heart_icon_bliner);
            hr_text = (TextView)v.findViewById(R.id.textView_hr);
            hr_text.setText("0");
            wavform = (WaveFormView1_1)v.findViewById(R.id.testing_wav);
            wavform.setBackgroundColor(MainActivity.getmContext().getResources().getColor(android.R.color.white));
            mPlaybackView = (WaveformView)v.findViewById(R.id.playbackWaveformView);
            mPlaybackView.setVisibility(View.GONE);
            donutProgress = (DonutProgress)v.findViewById(R.id.donut_progress);
            donutProgress.setFinishedStrokeColor(getResources().getColor(R.color.colorPrimaryDark));
            play_btn = (ImageButton)v.findViewById(R.id.btn_play);

            pause_button = (ImageButton)v.findViewById(R.id.btn_pause);
            reset_btn = (ImageButton)v.findViewById(R.id.btn_reset);
            speed_spinner = (Spinner)v.findViewById(R.id.speed_setting_spinner);
            history_btn = (ImageButton)v.findViewById(R.id.btn_history);
            session_btn = (ImageButton)v.findViewById(R.id.btn_session);
            settings_btn = (ImageButton)v.findViewById(R.id.btn_settings);

            donutProgress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!isRecording()){
                        NEW_FILE = true;
                        FILE_SAVED = false;
                        wavform.setBackgroundResource(0);
                        wavform.setVisibility(View.VISIBLE);
                        graph_timer = 0;
                        val =0;
                        if (run_updates !=null){
                            run_updates.interrupt();
                            run_updates = null;
                        }
                        run_updates = new Thread(){
                            @Override
                            public void run(){

                                Value_Updater();

                            }
                        };
                        run_updates.start();

                        graph_mHandler = new Handler();

                        com.sensicardiac.sensixvi.Stream.Stream.Start_Analog_Recording();}
                    else if (isRecording()){
                        com.sensicardiac.sensixvi.Stream.Stream.setRecording_busy(false);
                        run_updates.interrupt();
                    }

                }
            });
            play_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                if (FILE_SAVED){
                    if (!mPlaybackThread.playing()){

                        mPlaybackThread.startPlayback();
                    }
                }}
            });

            pause_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (FILE_SAVED){
                    mPlaybackThread.stopPlayback();}
                }
            });

            reset_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (FILE_SAVED){
                    mPlaybackThread.stopPlayback();}
                }
            });
            List<String> speeds = new ArrayList<String>();
            speeds.add("1x");
            speeds.add("0.5x");
            speeds.add("0.25x");
            speeds.add("2x");
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(MainActivity.getmContext(),
                    R.layout.spinner_item1, speeds);
            dataAdapter.setDropDownViewResource(R.layout.spinner_item1);
            speed_spinner.setAdapter(dataAdapter);
            speed_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    String Item = adapterView.getItemAtPosition(i).toString();
                    switch (Item){
                        case "1x":
                            speed = 1;
                            break;
                        case "0.5x":
                            speed = 2;
                            break;
                        case "0.25x":
                            speed = 3;
                            break;
                        case "2x":
                            speed = 4;
                            break;
                        default:
                            speed = 1;
                            break;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                    speed = 1;
                }
            });

            history_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Setpage(1);
                }
            });

            session_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Setpage(0);
                }
            });

            settings_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (FILE_SAVED){
                    DB_SHOW = true;
                    Setpage(4);}
                }
            });
            Log.d("FILE SAVED","<<<<<<<<<<<<<<<FILE SAVED>>>>>>>>>>>>>>"+FILE_SAVED);
            if (FILE_SAVED){
                com.sensicardiac.sensixvi.Controller.MainActivity.Playback_Audio_File_Graph();
                com.sensicardiac.sensixvi.Controller.MainActivity.mPlaybackView.setVisibility(View.VISIBLE);
                com.sensicardiac.sensixvi.Controller.MainActivity.wavform.setVisibility(View.GONE);
                MainActivity.hr_text.setText(String.valueOf(HR_FLASHER_VAL));
            }

            return  v;}


}
