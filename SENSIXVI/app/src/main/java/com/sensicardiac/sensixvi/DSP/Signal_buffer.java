
package com.sensicardiac.sensixvi.DSP;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Signal_buffer
{
    private Queue<Double> signal_buffer;
    private int length;

    public Signal_buffer(int length)
    {
        this.length=length;
        signal_buffer=new ConcurrentLinkedQueue<Double>();

    }
    public void set_length(int length)
    {
        this.length=length;
    }

    public void add_val(double x)
    {
        signal_buffer.add(x);

        if(signal_buffer.size()>length)
        {
            signal_buffer.poll();
        }
    }
    public Double[] getArray()
    {

        return signal_buffer.toArray(new Double[signal_buffer.size()]);
    }
    public void reset()
    {
    signal_buffer.clear();
    }
    public boolean isReady()
    {

    return Math.abs(signal_buffer.size()-length)<1;
    }
//    public double getVal(int i)
//    {
//        return signal_buffer.element(i);
//    }
}