package com.sensicardiac.sensixvi.Layout_fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sensicardiac.sensixvi.R;

/**
 * Created by Edward on 2017/02/20.
 */

public class menu_fragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        return inflater.inflate(R.layout.fragment_bot_menu,container,false);
    }
}
