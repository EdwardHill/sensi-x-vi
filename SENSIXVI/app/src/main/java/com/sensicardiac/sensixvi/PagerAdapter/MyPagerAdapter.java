package com.sensicardiac.sensixvi.PagerAdapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.sensicardiac.sensixvi.Controller.Main_Fragment;
import com.sensicardiac.sensixvi.Recording_Management.DB_Show_Item_fragment;
import com.sensicardiac.sensixvi.Recording_Management.Recording_List_Fragment;
import com.sensicardiac.sensixvi.Recording_Management.Recoring_Add;
import com.sensicardiac.sensixvi.Sharing.fragment_Share;

/**
 * Created by edward on 14/11/18.
 */
public class MyPagerAdapter extends FragmentStatePagerAdapter {
    public MyPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                //Fragement for Android Tab
                return new Main_Fragment();
            case 1:
                //Fragment for Ios Tab
                return new Recording_List_Fragment();
            case 2:
                //Fragment for Windows Tab
                return new Recoring_Add();
            case 3:
                //Fragment for Windows Tab
                return new fragment_Share();
            case 4:
                return new DB_Show_Item_fragment();

        }

        return null;
    }

    @Override
    public int getCount() {
        return 5;
    }
}
