package com.sensicardiac.sensixvi.DSP;

import android.util.Log;

import com.sensicardiac.sensixvi.DSP.DSP_Variables.*;


import java.nio.ByteBuffer;
import java.nio.ByteOrder;



/**
 * Created by davidfourie on 2016/09/16.
 */



public class uni_downsampler {
    private Filter antiAliasing;
    private int factor;
    private int count;
    ByteBuffer output;

    private int output_count;
    private int graph_count;
    private long graph_length;
    private int graph_update;
    private int[] graph;
    private Preprocessor ps;
    private long count2;




    public uni_downsampler(int graph_time,double update_time)
    {
        ps=new Preprocessor(1000,4000);
        graph=new int[graph_time*4000];
        for(int i=0;i<graph.length;i++)
        {
            graph[i]=0;
        }
        graph_update=(int)Math.round(update_time*4000);
        output=ByteBuffer.allocate(100*4000*2);
        output.order(ByteOrder.LITTLE_ENDIAN);
//        input=ByteBuffer.allocate(2);
        count=0;
        count2=0;
        output_count=0;
        double[] B= new double[3];
        double[] A= new double[3];
        switch(com.sensicardiac.sensixvi.Stream.Stream.getSampleRate()) {
            case 4000:
                factor=0;
                B[0]=0.09763107;
                B[1]=0.19526215;
                B[2]=0.09763107;

                A[0]=1;
                A[1]=-0.94280904;
                A[2]=0.33333333;

                antiAliasing=new Filter(A,B);
                break;
            case 8000:
                factor=1;
                B[0]=0.02995458;
                B[1]=0.05990916;
                B[2]=0.02995458;

                A[0]=1.00000000e+00;
                A[1]=-1.45424359;
                A[2]=0.57406192;
                antiAliasing=new Filter(A,B);
                break;
            case 16000:
                factor=3;
                B[0]=0.00844269;
                B[1]=0.01688539;
                B[2]=0.00844269;

                A[0]=1.00000000e+00;
                A[1]=-1.72377617;
                A[2]=0.75754694;
                antiAliasing=new Filter(A,B);

                break;
            case 32000:
                factor=7;
                B[0]=0.00225158;
                B[1]=0.00450317;
                B[2]=0.00225158;

                A[0]=1;
                A[1]=-1.86136115;
                A[2]=0.87036748;
                antiAliasing=new Filter(A,B);

                break;
            case 96000:
                factor=23;
                B[0]=0.00026165;
                B[1]=0.00052331;
                B[2]=0.00026165;

                A[0]=1;
                A[1]=-1.95372795;
                A[2]=0.95477456;
                antiAliasing=new Filter(A,B);
                break;
        }
//        Log.i("Setup","Factor: "+factor);

    }

    public void add_val(byte LSB, byte MSB)
    {
//        input.clear();
//        input.put(MSB);
//        input.put(LSB);
        ByteBuffer input=ByteBuffer.allocate(2);
        input.order(ByteOrder.LITTLE_ENDIAN);
        input.put(LSB);
        input.put(MSB);


        double val=antiAliasing.add_val((double)input.getShort(0));
//        count2++;
        if(count==factor)
        {
//            Log.d("Counter2","X: "+output_count+" Y"+val);
//
            //doStuffhere with Val
//            Log.d("Before","X: "+output_count+" Y"+val);
//            val=ps.add_val(val*100)*128;
            DSP_Variables.hrCalc.add_val(val);
            short val_out=(short)Math.round(val);
            output.putShort(2*output_count,val_out);

            //add data to graph
            graph[graph_count]=val_out;
            //Graph timer trigger
            if(graph_count%graph_update==0)
            {
//                Log.d("After","X: "+output_count+" Y:");
             com.sensicardiac.sensixvi.Stream.Stream.setGraph_data_ready(true);
            }

            graph_count++;
            //reset counter to overide
            if(graph_count==graph.length)
            {
                graph_count=0;
            }


            output_count++;
            count=0;
//            Log.d("Counter","val: "+val);

        }
        else
        {
            count++;
        }
    }
    public void reset()
    {
        count2=0;
        ps.reset();
        count=0;
        antiAliasing.reset();
        for(int i=0;i<graph.length;i++)
        {
            graph[i]=0;
        }
        graph_count=0;
        output.clear();
        output_count=0;

    }
    public int[] getGraph()
    {
        return graph;
    }
    public void forceSamplerate(int samplerate)
    {
        double[] B= new double[3];
        double[] A= new double[3];
        switch(samplerate) {
            case 4000:
                factor=0;
                B[0]=1;
                B[1]=2;
                B[2]=1;

                A[0]=1;
                A[1]=2;
                A[2]=1;

                antiAliasing=new Filter(A,B);
                break;
            case 8000:
                factor=1;
                B[0]=0.29289322;
                B[1]=0.58578644;
                B[2]=0.29289322;

                A[0]=1.00000000e+00;
                A[1]=-1.66533454e-16;
                A[2]=1.71572875e-01;
                antiAliasing=new Filter(A,B);
                break;
            case 16000:
                factor=3;
                B[0]=0.09763107;
                B[1]=0.19526215;
                B[2]=0.09763107;

                A[0]=1.00000000e+00;
                A[1]=-0.94280904;
                A[2]=0.33333333;
                antiAliasing=new Filter(A,B);

                break;
            case 32000:
                factor=7;
                B[0]=0.02995458;
                B[1]=0.05990916;
                B[2]=0.02995458;

                A[0]=1;
                A[1]=-1.45424359;
                A[2]=0.57406192;
                antiAliasing=new Filter(A,B);

                break;
            case 96000:
                factor=23;
                B[0]=0.00391613;
                B[1]=0.00783225;
                B[2]=0.00391613;

                A[0]=1;
                A[1]=-1.81534108;
                A[2]=0.83100559;
                antiAliasing=new Filter(A,B);
                break;
        }
    }
    public byte[] getByteArray()
    {
        Log.d("FileSave","Output_count "+output_count);
        byte[] out=new byte[output_count*2];
//        output.rewind();
//        output.get(out,0,output_count*2);
        for(int i=0;i<output_count*2;i++)
        {

            out[i]=output.get(i);
//            out3[i] = bb.getShort(0);
        }

        Log.d("FileSave","Length "+out.length);
        return out;
    }
    public short[] getShortArray()
    {

//        byte[] out=new byte[output_count*2];
        short[] out3=new short[output_count];
        for(int i=0;i<output_count;i++)
        {
            ByteBuffer bb = ByteBuffer.allocate(2);
            bb.order(ByteOrder.LITTLE_ENDIAN);
            bb.put(output.get(2*i));
            bb.put(output.get(2*i + 1));
            out3[i] = bb.getShort(0);
        }

//        Log.d("FileSave","Length "+out.length);
        return out3;
    }

}
