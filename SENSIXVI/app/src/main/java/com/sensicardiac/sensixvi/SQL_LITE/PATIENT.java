package com.sensicardiac.sensixvi.SQL_LITE;

/**
 * Created by Edward on 2017/02/21.
 */

public class PATIENT {

    private static int _id;
    private static String _Name;
    private static int _Age;
    private static String _P_Position;
    private static String _P_Apex_Disp;
    private static  String _P_Sound_File;
    private static String _P_HR_VAL;

    public PATIENT(){}
    public PATIENT(int id, String Name, int Age,String P_POSITION,String P_Apec_Disp,String P_SOUND_FILE, String P_HR_VAL){
        this._id = id;
        this._Name = Name;
        this._Age = Age;
        this._P_Position = P_POSITION;
        this._P_Apex_Disp = P_Apec_Disp;
        this._P_Sound_File = P_SOUND_FILE;
        this._P_HR_VAL = P_HR_VAL;
    }

    public PATIENT( String Name, int Age,String P_POSITION,String P_Apec_Disp,String P_SOUND_FILE, String P_HR_VAL){
        this._Name = Name;
        this._Age = Age;
        this._P_Position = P_POSITION;
        this._P_Apex_Disp = P_Apec_Disp;
        this._P_Sound_File = P_SOUND_FILE;
        this._P_HR_VAL = P_HR_VAL;
    }

    public static int get_id() {
        return _id;
    }

    public static void set_id(int _id) {
        PATIENT._id = _id;
    }


    public static String get_Name() {
        return _Name;
    }

    public static void set_Name(String _Name) {
        PATIENT._Name = _Name;
    }

    public static int get_Age() {
        return _Age;
    }

    public static void set_Age(int _Age) {
        PATIENT._Age = _Age;
    }
    public static String get_P_Position() {
        return _P_Position;
    }

    public static void set_P_Position(String _P_Position) {
        PATIENT._P_Position = _P_Position;
    }

    public static String get_P_Apex_Disp() {
        return _P_Apex_Disp;
    }

    public static void set_P_Apex_Disp(String _P_Apex_Disp) {
        PATIENT._P_Apex_Disp = _P_Apex_Disp;
    }

    public static String get_P_Sound_File() {
        return _P_Sound_File;
    }

    public static void set_P_Sound_File(String _P_Sound_File) {
        PATIENT._P_Sound_File = _P_Sound_File;
    }

    public static String get_PHrVal() {
        return _P_HR_VAL;
    }

    public static void set_PHrVal(String PHrVal) {
        _P_HR_VAL = PHrVal;
    }
}
