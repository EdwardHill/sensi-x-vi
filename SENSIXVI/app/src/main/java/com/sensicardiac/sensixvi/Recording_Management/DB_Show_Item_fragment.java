package com.sensicardiac.sensixvi.Recording_Management;

import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.sensicardiac.sensixvi.Controller.MainActivity;
import com.sensicardiac.sensixvi.R;
import com.sensicardiac.sensixvi.Recording_Management.utils.File_Util;
import com.sensicardiac.sensixvi.SQL_LITE.DB_Handler;
import com.sensicardiac.sensixvi.SQL_LITE.PATIENT;

import java.io.File;

import static com.sensicardiac.sensixvi.Controller.MainActivity.PATIENT_SELECTED;
import static com.sensicardiac.sensixvi.Controller.MainActivity.Setpage;
import static com.sensicardiac.sensixvi.Controller.MainActivity._p_age;
import static com.sensicardiac.sensixvi.Controller.MainActivity._p_apex;
import static com.sensicardiac.sensixvi.Controller.MainActivity._p_name;
import static com.sensicardiac.sensixvi.Controller.MainActivity._p_pos;
import static com.sensicardiac.sensixvi.Controller.MainActivity.set_DB_ITEM;
import static com.sensicardiac.sensixvi.Recording_Management.Recoring_Add.Cp_File;

/**
 * Created by Edward on 2017/02/23.
 */

public class DB_Show_Item_fragment  extends Fragment{



        //    public static Recoring_Add newInstance(){
//        Recoring_Add f = new Recoring_Add();
//        return f;
//    }
    private View vv;
    public static TextView db_name;
    public static TextView db_age;
    public static TextView db_pos;
    public static TextView db_apex;
    private Button cancel_db_show;
    private Button share_db_show;


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

            vv = inflater.inflate(R.layout.fragment_db_item,container,false);
            db_name = (TextView) vv.findViewById(R.id.textView_db_show_name);
            db_age = (TextView) vv.findViewById(R.id.textView_db_show_age);
            db_pos = (TextView) vv.findViewById(R.id.textView_db_show_pos);
            db_apex = (TextView) vv.findViewById(R.id.textView_db_apexdis);
            db_name.setText(_p_name);
            db_age.setText(_p_age);
            db_pos.setText(_p_pos);
            db_apex.setText(_p_apex);
            cancel_db_show = (Button) vv.findViewById(R.id.btn_cancel_db_show);
            share_db_show = (Button)vv.findViewById(R.id.btn_share_db_show);
            cancel_db_show.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Setpage(0);
                }
            });
            share_db_show.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String p_name = db_name.getText().toString();
                    File temp = new File(Environment.getExternalStorageDirectory() + "/sensi_x/tempfile.wav");
                    File to = new File(Environment.getExternalStorageDirectory() + "/sensi_x/"+ p_name + ".wav");
                    File_Util.copyFile(temp,to);
                    Cp_File = to;
                    Setpage(3);
                }
            });
            return vv;}





    }




