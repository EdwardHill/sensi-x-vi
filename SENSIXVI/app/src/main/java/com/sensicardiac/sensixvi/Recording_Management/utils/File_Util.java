package com.sensicardiac.sensixvi.Recording_Management.utils;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Edward on 2017/02/23.
 */

public class File_Util {
    public static void copyFile(File inputFile, File outputPath) {

        InputStream in = null;
        OutputStream out = null;
        try {
            File outFile = outputPath;
            //create output directory if it doesn't exist
            if (!outFile.exists()){
                try {
                    outFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (outFile.exists()){
                outFile.delete();
                try {
                    outFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


            in = new FileInputStream(inputFile);
            out = new FileOutputStream(outFile);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;

            // write the output file (You have now copied the file)
            out.flush();
            out.close();
            out = null;

        }  catch (FileNotFoundException fnfe1) {
            Log.e("tag", fnfe1.getMessage());
        }
        catch (Exception e) {
            Log.e("tag", e.getMessage());
        }

    }

    public static void deleteFile(String inputFile) {
        try {
            // delete the original file
            new File(inputFile).delete();
        }
        catch (Exception e) {
            Log.e("tag", e.getMessage());
        }
    }

    public static void Del_Directory(){
        File dir = new File(Environment.getExternalStorageDirectory() + "/sensi_x");
        if (dir.isDirectory())
        {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++)
            {
                new File(dir, children[i]).delete();
            }
        }
    }

}


