package com.sensicardiac.sensixvi.Recording_Management;

import android.app.DialogFragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.sensicardiac.sensixvi.Controller.MainActivity;
import com.sensicardiac.sensixvi.R;
import com.sensicardiac.sensixvi.Recording_Management.utils.File_Util;
import com.sensicardiac.sensixvi.SQL_LITE.DB_Handler;
import com.sensicardiac.sensixvi.SQL_LITE.PATIENT;
import com.sensicardiac.sensixvi.Stream.Stream;

import java.io.File;

import static com.sensicardiac.sensixvi.Controller.MainActivity.FILE_SAVED;
import static com.sensicardiac.sensixvi.Controller.MainActivity.NEW_FILE;
import static com.sensicardiac.sensixvi.Controller.MainActivity.PATIENT_SELECTED;
import static com.sensicardiac.sensixvi.Controller.MainActivity.Setpage;
import static com.sensicardiac.sensixvi.Recording_Management.Recording_List_Fragment.list_init;

/**
 * Created by Edward on 2017/02/22.
 */

public class Recoring_Add extends Fragment {

//    public static Recoring_Add newInstance(){
//        Recoring_Add f = new Recoring_Add();
//        return f;
//    }
    public Button close;
    public  Button save;
    public  Button share;
    public static EditText name;
    public static EditText age;
    public static Spinner position;
    public static Spinner apex_disp;
    public View vv;
    public static String p_name;
    public static File Cp_File;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
       vv = inflater.inflate(R.layout.fragment_sql_input,container,false);
        close = (Button) vv.findViewById(R.id.btn_cancel_add_recording);
        save = (Button) vv.findViewById(R.id.btn_rec_save_recording);
        share = (Button)vv.findViewById(R.id.btn_share_recording);
        name = (EditText)vv.findViewById(R.id.editText_name);
        age = (EditText)vv.findViewById(R.id.editText_age);
        position = (Spinner)vv.findViewById(R.id.spinner_position);
        apex_disp = (Spinner) vv.findViewById(R.id.spinner_apex_disp);
        if(NEW_FILE){
        name.setText("");
        age.setText("");}
        else  if (FILE_SAVED && !NEW_FILE)
        {
            DB_Handler db = new DB_Handler(MainActivity.getmContext());
            PATIENT p = db.getPatient(PATIENT_SELECTED);
            String P_name = p.get_Name();
            name.setText(P_name);
            age.setText(String.valueOf(p.get_Age()));
            String ap = p.get_P_Apex_Disp();
            if (ap.contains("YES")){
                apex_disp.setSelection(0);
            }
            else {
                apex_disp.setSelection(1);
            }
            String pos = p.get_P_Position();
            switch (pos){
                case ("Aortic"):
                    position.setSelection(0);
                    break;
                case ("Tricuspid"):
                    position.setSelection(1);
                    break;
                case ("Mitral"):
                    position.setSelection(2);
                    break;
                case ("Apex"):
                    position.setSelection(3);
                    break;
            }
            db.close();
        }


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Setpage(0);
            }
        });
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean empty = false;
                String name_text = name.getText().toString().trim();
                String age_text = age.getText().toString().trim();
                if (name_text.isEmpty()||name_text.length() == 0 || name_text.equals("") || name_text == null ){
                    empty = true;
                }
                if (age_text.isEmpty()||age_text.length() == 0 || age_text.equals("") || age_text == null ){
                    empty = true;
                }
                if (!empty){
                    p_name = name_text;
                    File temp = new File(Environment.getExternalStorageDirectory() + "/sensi_x/tempfile.wav");
                    File to = new File(Environment.getExternalStorageDirectory() + "/sensi_x/"+ name_text + ".wav");
                    File_Util.copyFile(temp,to);
                    Cp_File = to;
                Setpage(3);}
                else {
                    Toast.makeText(MainActivity.getmContext(),"Either the Name or Age fields are empty, please add the correct value and try again!", Toast.LENGTH_LONG).show();
                }
              //  Export_to_Email();
            }

        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean empty = false;
                String name_text = name.getText().toString().trim();
                String age_text = age.getText().toString().trim();
                if (name_text.isEmpty()||name_text.length() == 0 || name_text.equals("") || name_text == null ){
                    empty = true;
                }
                if (age_text.isEmpty()||age_text.length() == 0 || age_text.equals("") || age_text == null ){
                    empty = true;
                }
                if (!empty){
                PATIENT p = new PATIENT();
                p.set_Name(name.getText().toString());
                p.set_Age(Integer.parseInt(age.getText().toString()));
                int i = position.getSelectedItemPosition();
                String[] pos_array = MainActivity.getmContext().getResources().getStringArray(R.array.Position);
                String  pos = String.valueOf(pos_array[i]);
                p.set_P_Position(pos);
                    int k = apex_disp.getSelectedItemPosition();
                String[] disp_array = MainActivity.getmContext().getResources().getStringArray(R.array.Displaced);
                String disp = String.valueOf(disp_array[k]);

                p.set_P_Apex_Disp(disp);
                p.set_P_Sound_File(com.sensicardiac.sensixvi.Recording_Management.utils.Encryption.Encode());
                    p.set_PHrVal(String.valueOf(MainActivity.HR_FLASHER_VAL));
                DB_Handler db = new DB_Handler(MainActivity.getmContext());
                db.addPatient(p);
                    list_init();
                    Setpage(0);
                    db.close();
                }
                else {
                    Toast.makeText(MainActivity.getmContext(),"Either the Name or Age fields are empty, please add the correct value and try again!", Toast.LENGTH_LONG).show();
                    }

            }
        });
        populate_fields();
     return vv;}

    public void Export_to_Email(){

        boolean empty = false;
        String name_text = name.getText().toString().trim();
        String age_text = age.getText().toString().trim();
        if (name_text.isEmpty()||name_text.length() == 0 || name_text.equals("") || name_text == null ){
            empty = true;
        }
        if (age_text.isEmpty()||age_text.length() == 0 || age_text.equals("") || age_text == null ){
            empty = true;
        }
        if (!empty){
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_SUBJECT,"Sound file");
            intent.putExtra(Intent.EXTRA_TEXT,"Sound file brought to you from SensiCardiac Android application. Recorded with Thinklabs One Stethoscope.");
            File temp = new File(Environment.getExternalStorageDirectory() + "/sensi_x/tempfile.wav");
            File to = new File(Environment.getExternalStorageDirectory() + "/sensi_x/"+ name_text + ".wav");
            p_name = name_text;
            temp.renameTo(to);
            Uri uri = Uri.fromFile(to);
            intent.putExtra(Intent.EXTRA_STREAM, uri);
            startActivity(Intent.createChooser(intent, "Send email..."));
            Toast.makeText(MainActivity.getmContext(),"Opening email application.", Toast.LENGTH_LONG).show();
        }
        else {
            Toast.makeText(MainActivity.getmContext(),"Either the Name or Age fields are empty, please add the correct value and try again!", Toast.LENGTH_LONG).show();
        }


    }

    public static void populate_fields(){


    }

    }


