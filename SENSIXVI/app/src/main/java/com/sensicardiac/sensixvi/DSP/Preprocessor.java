 
package com.sensicardiac.sensixvi.DSP;
public class Preprocessor{
private int length;
private MovingAverage ma;
private Normaliser norm;

    public Preprocessor(int length,int sample_rate)
    {

    ma=new MovingAverage(length);
    this.length=length;
    norm=new Normaliser(0.95,sample_rate,1);
    }
    public void reset()
    {

    ma.reset();
    norm.reset();
    }

    public double add_val(double x)
    {
    ma.add_val(x);
    double ave=ma.getMovingAverage();
    return norm.add_val(x-ave);

    }
}