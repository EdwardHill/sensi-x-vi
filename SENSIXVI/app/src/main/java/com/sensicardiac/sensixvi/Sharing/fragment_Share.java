package com.sensicardiac.sensixvi.Sharing;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.sensicardiac.sensixvi.Controller.MainActivity;
import com.sensicardiac.sensixvi.R;

import java.io.File;

import static com.sensicardiac.sensixvi.Controller.MainActivity.DB_SHOW;
import static com.sensicardiac.sensixvi.Controller.MainActivity.HR_FLASHER_VAL;
import static com.sensicardiac.sensixvi.Controller.MainActivity.Setpage;
import static com.sensicardiac.sensixvi.Controller.MainActivity.getmContext;
import static com.sensicardiac.sensixvi.Recording_Management.Recoring_Add.Cp_File;

/**
 * Created by Edward on 2017/02/23.
 */

public class fragment_Share extends Fragment {
    public View vv;
    private ImageButton whatsapp;
    private ImageButton email;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        vv = inflater.inflate(R.layout.fragment_share,container,false);
        whatsapp = (ImageButton)vv.findViewById(R.id.imageButton_whatsapp);
        email = (ImageButton)vv.findViewById(R.id.imageButton_gmail);
        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean installed = appInstalledOrNot("com.whatsapp");
                if(installed) {
                Uri uri = Uri.fromFile(Cp_File);
                Intent share = new Intent();
                share.setAction(Intent.ACTION_SEND);
                share.putExtra(Intent.EXTRA_STREAM, uri);
                share.setPackage("com.whatsapp");
                MainActivity.getmContext().startActivity(share);
                Setpage(2);}
                else {
                    final Handler handler = new Handler();
                    Toast.makeText(MainActivity.getmContext(),"App is not currently installed on your device",Toast.LENGTH_LONG).show();

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (!DB_SHOW){
                            Setpage(2);}
                            else {Setpage(4);}
                        }
                    }, 1000);

                }
            }
        });
        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT,"Sound file");
                intent.putExtra(Intent.EXTRA_TEXT,"Sound file brought to you from SensiCardiac Android application. Recorded with Thinklabs One Stethoscope.");
                Uri uri = Uri.fromFile(Cp_File);
                intent.putExtra(Intent.EXTRA_STREAM, uri);
                startActivity(Intent.createChooser(intent, "Send email..."));
                Toast.makeText(MainActivity.getmContext(),"Opening email application.", Toast.LENGTH_LONG).show();
                if (!DB_SHOW){
                    Setpage(2);}
                else {Setpage(4);}
            }
        });
        return vv;
    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getmContext().getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        }
        catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }
}
