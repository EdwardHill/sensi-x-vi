//package com.sensicardiac.sensi_module.dsp;

/**
 * Created by davidfourie on 16/05/25.
 */
package com.sensicardiac.sensixvi.DSP;
public class Normaliser {
    private double d;
    private double max;
    private int n;
    private int count;


    public Normaliser(double decrease,int sample_rate,double time_diff) {
        this.d = decrease;
        max=1;
        this.n=(int)Math.round(sample_rate*time_diff);
        this.count=0;

    }
    public void reset()
    {
        this.max=1;
        this.count=0;
    }
    public double getMax()
    {
        return this.max;
    }
    public double add_val(double x)
    {
        if(Math.abs(x)>max)
        {
            max=Math.abs(x);
        }
        if(count==n)
        {
            if(max>1)
            {
                max*=d;
            }
            count=0;
        }
        else
        {
            count++;
        }
        return x/max;
    }

}
