package com.sensicardiac.sensixvi.Recording_Management.utils;

import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Edward on 2017/02/22.
 */

public class Encryption {

    public static String Encode(){
    File temp = new File(Environment.getExternalStorageDirectory() + "/sensi_x/tempfile.wav");
        String encoded = "";
        try {
            byte[] bytes = FileUtils.readFileToByteArray(temp);
            encoded = Base64.encodeToString(bytes, 0);
            Log.d("~~~~~~~~ Encoded: ", encoded);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return encoded;
    }

    public static void decode(String b64String){
        byte[] decoded = Base64.decode(b64String, 0);
        try {
            File temp = new File(Environment.getExternalStorageDirectory() + "/sensi_x/tempfile.wav");
            File dir = new File(Environment.getExternalStorageDirectory() + "/sensi_x");
            if (!dir_exists(dir.getPath())){
                File directory = new File(String.valueOf(dir));
                directory.mkdirs();
            }
            if (!temp.exists()){
                try {
                    temp.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (temp.exists()){
                temp.delete();
                try {
                    temp.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


            FileOutputStream os = new FileOutputStream(temp, true);
            os.write(decoded);
            os.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    public static boolean dir_exists(String dir_path)
    {
        boolean ret = false;
        File dir = new File(dir_path);
        if(dir.exists() && dir.isDirectory())
            ret = true;
        return ret;
    }


}
