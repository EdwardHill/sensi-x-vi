package com.sensicardiac.sensixvi.Recording_Management;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.sensicardiac.sensixvi.Controller.MainActivity;
import com.sensicardiac.sensixvi.R;
import com.sensicardiac.sensixvi.Recording_Management.utils.Encryption;
import com.sensicardiac.sensixvi.SQL_LITE.DB_Handler;
import com.sensicardiac.sensixvi.SQL_LITE.PATIENT;

import java.util.ArrayList;
import java.util.List;

import dalvik.annotation.TestTargetClass;

import static android.webkit.ConsoleMessage.MessageLevel.LOG;
import static com.sensicardiac.sensixvi.Controller.MainActivity.FILE_SAVED;
import static com.sensicardiac.sensixvi.Controller.MainActivity.HR_FLASHER_VAL;
import static com.sensicardiac.sensixvi.Controller.MainActivity.NEW_FILE;
import static com.sensicardiac.sensixvi.Controller.MainActivity.PATIENT_SELECTED;
import static com.sensicardiac.sensixvi.Controller.MainActivity.Setpage;
import static com.sensicardiac.sensixvi.Controller.MainActivity.set_DB_ITEM;

/**
 * Created by Edward on 2017/02/22.
 */

public class Recording_List_Fragment extends Fragment {
    public static ListView rec_list;
    public static ArrayAdapter<String> adapter;


//    public static Recording_List_Fragment newInstance(){
//        Recording_List_Fragment f = new Recording_List_Fragment();
//        return f;
//    }
    @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.fragment_recording_list,container,false);


        rec_list = (ListView) v.findViewById(R.id.listview_recording);
        rec_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                DB_Handler db = new DB_Handler(MainActivity.getmContext());
                PATIENT p = db.getPatient(i+1);
                PATIENT_SELECTED = p.get_id();
                Encryption.decode(p.get_P_Sound_File());
                HR_FLASHER_VAL = Integer.parseInt(p.get_PHrVal());
                NEW_FILE = false;
                MainActivity.FILE_SAVED = true;
                com.sensicardiac.sensixvi.Controller.MainActivity.mPlaybackView.setVisibility(View.GONE);
                com.sensicardiac.sensixvi.Controller.MainActivity.wavform.setVisibility(View.VISIBLE);
                db.close();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        com.sensicardiac.sensixvi.Controller.MainActivity.Playback_Audio_File_Graph();
                        com.sensicardiac.sensixvi.Controller.MainActivity.mPlaybackView.setVisibility(View.VISIBLE);
                        com.sensicardiac.sensixvi.Controller.MainActivity.wavform.setVisibility(View.GONE);
                        MainActivity.hr_text.setText(String.valueOf(HR_FLASHER_VAL));
                        //    com.sensicardiac.sensixvi.Controller.MainActivity.sv.setSaving(true);

                        Log.d("FILE SAVED","<<<<<<<<<<<<<<<FILE SAVED>>>>>>>>>>>>>>"+MainActivity.FILE_SAVED);
                        set_DB_ITEM();
                        Setpage(0);
                    }
                }, 500);


            }
        });
        Button close = (Button) v.findViewById(R.id.close_record_list);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Setpage(0);
            }
        });
        list_init();
            return v;
        }

    public static void list_init(){
        DB_Handler db = new DB_Handler(MainActivity.getmContext());
        List<PATIENT> patients = db.getAllPatients();
        ArrayList<String> p = new ArrayList<String>();

        for (int x = 1;x < patients.size()+1;x++ ) {
            PATIENT cn = db.getPatient(x);
            p.add("Recording: "+cn.get_id() + " ,Name: " + cn.get_Name());
            String log = "Id: "+cn.get_id()+" ,Name: " + cn.get_Name()+ " ,Age: " + cn.get_Age()+" ,Position: "+cn.get_P_Position()+" ,Disp: " + cn.get_P_Apex_Disp() + " ,Sound: " + cn.get_P_Sound_File();
            // Writing Contacts to log
            Log.d("Name: ", log);
        }
        adapter =new ArrayAdapter<String >(MainActivity.getmContext(),R.layout.list_black_text,R.id.list_content,p);
        rec_list.setAdapter(adapter);
        db.close();
    }

}

