package com.sensicardiac.sensixvi.SQL_LITE;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Edward on 2017/02/21.
 */

public class DB_Handler extends SQLiteOpenHelper {
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "PatientManager";

    //table name
    private static final String TABLE_PATIENTS = "patients";

    // Patient Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_AGE = "age";
    private static final String KEY_P_POSITION = "position";
    private static final String KEY_P_APEX_DIPS = "apex_disp";
    private static final String KEY_P_SOUND_FILE = "sound_file";
    private static final String KEY_P_HR_VAL = "hrval";

    public DB_Handler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //Patient Table
        String CREATE_PATIENT_TABLE = "CREATE TABLE " + TABLE_PATIENTS + "(" +
                KEY_ID + " INTEGER PRIMARY KEY," +
                KEY_NAME + " TEXT," +
                KEY_AGE + " TEXT," +
                KEY_P_POSITION + " TEXT," +
                KEY_P_APEX_DIPS + " TEXT," +
                KEY_P_SOUND_FILE + " TEXT," +
                KEY_P_HR_VAL +" TEXT" + ")";
        sqLiteDatabase.execSQL(CREATE_PATIENT_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_PATIENTS);

        // Create tables again
        onCreate(sqLiteDatabase);
    }

    public void addPatient(PATIENT p) {
        SQLiteDatabase db = this.getWritableDatabase();


        ContentValues values = new ContentValues();
        values.put(KEY_NAME, PATIENT.get_Name());

        values.put(KEY_AGE, String.valueOf(PATIENT.get_Age()));
        values.put(KEY_P_POSITION, PATIENT.get_P_Position());
        values.put(KEY_P_APEX_DIPS, PATIENT.get_P_Apex_Disp());
        values.put(KEY_P_SOUND_FILE, PATIENT.get_P_Sound_File());
        values.put(KEY_P_HR_VAL, PATIENT.get_PHrVal());
        db.insert(TABLE_PATIENTS, null, values);
        db.close();
    }


    public PATIENT getPatient(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_PATIENTS, new String[]{KEY_ID,
                        KEY_NAME, KEY_AGE, KEY_P_POSITION, KEY_P_APEX_DIPS, KEY_P_SOUND_FILE, KEY_P_HR_VAL}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        PATIENT patient = new PATIENT(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), Integer.parseInt(cursor.getString(2)), cursor.getString(3), cursor.getString(4), cursor.getString(5),cursor.getString(6));
        // return Patient
        return patient;
    }

    public List<PATIENT> getAllPatients() {
        List<PATIENT> patientList = new ArrayList<PATIENT>();
        String selectQuery = "SELECT  * FROM " + TABLE_PATIENTS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                PATIENT patient = new PATIENT();
                patient.set_id(Integer.parseInt(cursor.getString(0)));
                patient.set_Name(cursor.getString(1));
                patient.set_Age(Integer.parseInt(cursor.getString(2)));
                patient.set_P_Position(cursor.getString(3));
                patient.set_P_Apex_Disp(cursor.getString(4));
                patient.set_P_Sound_File(cursor.getString(5));
                patient.set_PHrVal(cursor.getString(6));
                patientList.add(patient);
            } while (cursor.moveToNext());
        }
        return patientList;
    }

    public int getPatientCount(){
        String countQuery = "SELECT  * FROM " + TABLE_PATIENTS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

    public int updatePatient(PATIENT patient) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, PATIENT.get_Name());
        values.put(KEY_AGE, PATIENT.get_Age());
        values.put(KEY_P_POSITION, PATIENT.get_P_Position());
        values.put(KEY_P_APEX_DIPS, PATIENT.get_P_Apex_Disp());
        values.put(KEY_P_SOUND_FILE, PATIENT.get_P_Sound_File());
        values.put(KEY_P_HR_VAL, PATIENT.get_PHrVal());

        // updating row
        return db.update(TABLE_PATIENTS, values, KEY_ID + " = ?",
                new String[] { String.valueOf(patient.get_id()) });
    }


    // Deleting single patient
    public void deletePatient(PATIENT contact) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_PATIENTS, KEY_ID + " = ?",
                new String[] { String.valueOf(contact.get_id()) });
        db.close();
    }


}

