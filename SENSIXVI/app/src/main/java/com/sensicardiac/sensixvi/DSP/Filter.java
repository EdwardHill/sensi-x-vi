/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//package com.sensicardiac.sensi_module.dsp;
package com.sensicardiac.sensixvi.DSP;

public class Filter {
	
	private double[] x={0,0,0};
	private double[] y={0,0,0};
	private double[] B={0,0,0};
	private double[] A= {0,0,0};
	public enum filterType{
		lowpass30,lowpass500,bandpass200_700,bandpass5_60,lowpass5,lowpass10,bandpass1_5, chebyBand10_20,bandpass50_150
	}

	public Filter(double[] a, double[] b) {
		this.A = a;
		this.B = b;
	}
	public Filter(filterType in)
	{
		switch(in)
		{
			case lowpass30:
				B[0]=6.10061788e-05;
				B[1]=1.22012358e-04;
				B[2]=6.10061788e-05;

				A[0]=1;
				A[1]=-1.97778648;
				A[2]=0.97803051;

				break;
			case lowpass500:
				B[0]=0.09763107;
				B[1]=0.19526215;
				B[2]=0.09763107;

				A[0]=1;
				A[1]=-0.94280904;
				A[2]=0.33333333;
				break;
			case bandpass200_700:

				B[0]=0.29289322;
				B[1]=0;
				B[2]=-0.29289322;

				A[0]=1;
				A[1]=-1.1639790;
				A[2]=0.41421356;
				break;
			case bandpass5_60:

				B[0]=0.0414329;
				B[1]=0;
				B[2]=-0.0414329;

				A[0]=1;
				A[1]=-1.91642425;
				A[2]=0.9171342;
				break;
			case lowpass5:

				B[0]=1.53360084e-05;
				B[1]=3.06720167e-05;
				B[2]=-1.53360084e-05;

				A[0]=1;
				A[1]=-1.91642425;
				A[2]=0.9171342;
				break;
			case lowpass10:

				B[0]=6.10061788e-05;
				B[1]=1.22012358e-04;
				B[2]=-6.10061788e-05;

				A[0]=1;
				A[1]=-1.91642425;
				A[2]=0.9171342;
				break;

			case bandpass1_5:

				B[0]=0.00701909;
				B[1]=0;
				B[2]=0.00701909;

				A[0]=1;
				A[1]=-1.98593733;
				A[2]=0.98596183;
				break;
			case chebyBand10_20:
				B[0]=0.00634708;
				B[1]=0;
				B[2]=-0.00634708;

				A[0]=1;
				A[1]=-1.9868155;
				A[2]=0.98730584;
				break;
			case bandpass50_150:
				B[0]=0.07295966;
				B[1]=0;
				B[2]=-0.07295966;

				A[0]=1;
				A[1]=-1.83691648;
				A[2]=0.85408069;
				break;
				
				
			
				

		}
	}
	public void reset()
	{
		for(int i=0;i<3;i++) {
			x[i] = 0;
			y[i] = 0;
		}
	}
	public double add_val(double data)
	{
		//Shift x values
		x[2]=x[1];
		x[1]=x[0];
		x[0]=data;

		//shift y values
		y[2]=y[1];
		y[1]=y[0];

		//convolution
		y[0]=B[0]*x[0];
		y[0]+=B[1]*x[1]-A[1]*y[1];
		y[0]+=B[2]*x[2]-A[2]*y[2];

		return y[0];


	}
}
