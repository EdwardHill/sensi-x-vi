package com.sensicardiac.sensixvi.DSP;

import java.util.Timer;
import java.util.TimerTask;

//import java.lang.Thread;

public class HR_Processor{
    private Timer timer;
    private int counter;

    private Filter anti_aliasing;
    private doProcess mt;
    public RunningStatistics stats;
    public HR_Processor()
    {

    mt=new doProcess();
    anti_aliasing=new Filter(Filter.filterType.lowpass500);
    timer= new Timer();
    stats=new RunningStatistics();
    Process_signal.reset();
    Process_signal.setHR(50,120);
    Process_signal.reset();
    timer.schedule(new doProcess(),1,50);
//    mt.start();
    double last_hr;


    }
    public void reset()
    {
         anti_aliasing.reset();
        Process_signal.reset();
        counter=0;
        stats.reset();
    }
    public void add_val(double x)
    {
        if(counter==0)
        {
            Process_signal.l_add(anti_aliasing.add_val(x)) ;
            counter++;
//
        }
        else
        {
            double temp=anti_aliasing.add_val(x);
            if(counter==3)
            {
               counter=0;
            }
            else
            {
            counter++;
            }
        }
        stats=Process_signal.stats;

    }


    public class doProcess extends TimerTask{
            public void run()
            {
                Process_signal.process();
            }

    }

}
