/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sensicardiac.sensixvi.DSP;

/**
 *
 * @author davidfourie
 */
public class RunningStatistics {
	private int N;
	private double average;
	private double variance;
	private double max;
	private double min;
	private MovingAverage avg;
	
	
	public RunningStatistics()
	{
		avg=new MovingAverage(5);
		this.reset();
		
	}
	public void reset()
	{
		N=0;
		average=0;
		variance=0;
		max=0;
		min=90;
		avg.reset();
	}
	public void add_val(double x)
	{
		
		N++;
		
		avg.add_val(x);
		double oldavg = average;
		average = oldavg + (x - oldavg)/N;
//		System.out.println("Moving average "+avg.getMovingAverage());
		variance = variance + (x-average)*(x-oldavg);
		if(x>max)
		{
			max=x;
		}
		if(x<min)
		{
			min=x;
		}
	}

	public double getAverage() {
		return Math.round(average);
	}

	public double getStandardDeviation() {
		return Math.round(Math.sqrt(variance/N)*10)/10;
	}

	public double getMax() {
		return Math.round(max);
	}

	public double getMin() {
		return Math.round(min);
	}
	public double getMovingAverage()
	{
		return Math.round(avg.getMovingAverage());
	}
	
	
	
}
